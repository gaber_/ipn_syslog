obj-m += ipn_syslog.o

IPN_PATH   ?= $(PWD)/ipn
KERNEL_VER ?= $(shell uname -r)

EXTRA_CFLAGS += -I$(PWD) -DIPN_STEALING -DSYSLOG_NAMES

.PHONY: all modules eg clean

all: modules ipn_syslogd ipn_syslog.so originator collector

$(IPN_PATH):
	svn co https://vde.svn.sourceforge.net/svnroot/vde/trunk/ipn $(IPN_PATH)
	make -C ipn

modules: ipn_syslog.c af_ipn.h Module.symvers
	$(MAKE) -C /lib/modules/$(KERNEL_VER)/build M=$(PWD) modules

af_ipn.h: $(IPN_PATH)
	cp $(IPN_PATH)/$@ .

Module.symvers: $(IPN_PATH)
	cp $(IPN_PATH)/$@ .

ipn_syslogd: ipn_syslogd.c
	$(CC) $(EXTRA_CFLAGS) $(LDFLAGS) -o $@ $^

ipn_syslog.so: ipn_syslogso.c
	$(CC) $(EXTRA_CFLAGS) $(LDFLAGS) -fPIC -shared -o $@ $^

originator: eg/originator.c
	$(CC) $(EXTRA_CFLAGS) $(LDFLAGS) -o $@ $^

collector: eg/collector.c
	$(CC) $(EXTRA_CFLAGS) $(LDFLAGS) -o $@ $^

clean:
	$(MAKE) -C /lib/modules/$(KERNEL_VER)/build M=$(PWD) clean
	$(RM) af_ipn.h ipn_syslogd ipn_syslog.so originator collector collector_ng
