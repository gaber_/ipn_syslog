#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>

#include <pthread.h>

#include <syslog.h>

#include "af_ipn.h"
#include "ipn_syslog.h"

static int   log_fd       = -1;
static int   log_facility = LOG_USER;
static char *log_ident;

extern char *__progname;

void openlog(const char *ident, int option, int facility) {
	int rc, fd;

	if (log_fd != -1)
		return;

	fd = socket(AF_IPN, SOCK_RAW, IPN_SYSLOG);

	struct sockaddr_un ipn_dest = {
		.sun_family = AF_IPN,
		.sun_path   = IPN_SYSLOG_SOCK
	};

	rc = shutdown(fd, SHUT_RD);

	if (rc < 0) {
		perror("shutdown()");
		goto error;
	}

	rc = connect(fd, (struct sockaddr *) &ipn_dest, sizeof(ipn_dest));

	if (rc < 0) {
		perror("connect()");
		goto error;
	}

	if (ident == NULL)
		ident = __progname;

	log_ident = strdup(ident);

	if (facility >= 0)
		log_facility = facility;

	log_fd = fd;

	return;

error:
	close(fd);
	return;
}

void vsyslog(int pri, const char *format, va_list args) {
	int rc;

	char *fmt;

	int   log_len;
	char *log_msg;

	time_t now;
	struct tm *now_tm;
	char now_str[16];

	if (log_fd == -1)
		return;

	pri |= log_facility;

	time(&now);
	strftime(now_str, 16, "%h %e %T", localtime(&now));

	asprintf(&fmt, "<%d>%s %s[%d]: %s",
		pri, now_str, log_ident, getpid(), format);

	log_len = vasprintf(&log_msg, fmt, args);

	rc = send(log_fd, log_msg, log_len, 0);

	if (rc < 0) {
		perror("send()");
		return;
	}
}

void syslog(int pri, const char *format, ...) {
	va_list args;

	va_start(args, format);
	vsyslog(pri, format, args);
	va_end(args);
}

void closelog() {
	close(log_fd);
	log_fd = -1;
	free(log_ident);
}
