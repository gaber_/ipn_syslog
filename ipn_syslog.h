#define IPN_SYSLOG 2
#define IPN_SYSLOG_SOCK "/tmp/ipn_syslog.sock"

#define IPN_SYSLOG_MAXRCV 4096

#define IPN_IOCTL_FACILITY 4444
#define IPN_IOCTL_SEVERITY 5555

#define syslog_name(NAME, NAMES, DEFAULT) ({		\
	int i, res = DEFAULT;				\
							\
	for (i = 0; NAMES[i].c_name != NULL; i++) {	\
		if (strcmp(NAME, NAMES[i].c_name) == 0)	\
			res = NAMES[i].c_val;		\
	}						\
							\
	res;						\
})
