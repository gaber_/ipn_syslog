#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>

#include <sys/ioctl.h>

#include <syslog.h>
#include <getopt.h>

#include "af_ipn.h"
#include "ipn_syslog.h"

static int setup_ipn_fd() {
	int rc, fd = socket(AF_IPN, SOCK_RAW, IPN_SYSLOG);

	struct sockaddr_un ipn_dest = {
		.sun_family = AF_IPN,
		.sun_path   = IPN_SYSLOG_SOCK
	};

	rc = shutdown(fd, SHUT_WR);

	if (rc < 0) {
		perror("shutdown()");
		exit(-1);
	}

	rc = connect(fd, (struct sockaddr *) &ipn_dest, sizeof(ipn_dest));

	if (rc < 0) {
		perror("connect()");
		exit(-1);
	}

	return fd;
}

int main(int argc, char *argv[]) {
	int rc, opt, i;
	char msg[IPN_SYSLOG_MAXRCV + 1];

	int facility = LOG_USER, severity = LOG_NOTICE;

	int fd = setup_ipn_fd();

	/* parse args */
	static struct option opts[] = {
		{ "facility", required_argument, 0, 'f' },
		{ "severity", required_argument, 0, 's' },
		{ 0,          0,                 0, 0   }
	};

	while ((opt = getopt_long(argc, argv, "f:s:", opts, &i)) >= 0) {
		switch (opt) {
			case 'f': {
				int facility = syslog_name(
					optarg, facilitynames, LOG_USER
				);

				rc = ioctl(fd, IPN_IOCTL_FACILITY, facility);

				if (rc < 0) {
					perror("ioctl()");
					exit(-1);
				}

				break;
			}

			case 's': {
				int severity = syslog_name(
					optarg, prioritynames, LOG_NOTICE
				);

				rc = ioctl(fd, IPN_IOCTL_SEVERITY, severity);

				if (rc < 0) {
					perror("ioctl()");
					exit(-1);
				}

				break;
			}
		}
	}

	/* recv loop */
	while ((rc = recv(fd, msg, IPN_SYSLOG_MAXRCV, 0))) {
		if (rc < 0) {
			perror("recv()");
			exit(-1);
		}
		msg[rc]='\0';
		puts(msg);
	}

	close(fd);

	return 0;
}
