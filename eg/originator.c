#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <getopt.h>
#include <syslog.h>

#include "ipn_syslog.h"

int main(int argc, char *argv[]) {
	int opt, i;
	char *msg = NULL;
	int facility = LOG_USER, severity = LOG_NOTICE;

	/* parse args */
	static struct option opts[] = {
		{ "facility", required_argument, 0, 'f' },
		{ "severity", required_argument, 0, 's' },
		{ "message",  required_argument, 0, 'm' },
		{ 0,          0,                 0, 0   }
	};

	while ((opt = getopt_long(argc, argv, "f:s:m:", opts, &i)) >= 0) {
		switch (opt) {
			case 'f':
				facility = syslog_name(
					optarg, facilitynames, LOG_USER
				);
				break;

			case 's':
				severity = syslog_name(
					optarg, prioritynames, LOG_NOTICE
				);
				break;
			case 'm':
				msg = strdup(optarg);
				break;
		}
	}

	if (msg == NULL) {
		fprintf(stderr, "Please provide a log message\n");
		return -1;
	}

	openlog(argv[0], LOG_CONS | LOG_PID | LOG_NDELAY, facility);
	syslog(severity, "%s", msg);
	closelog();

	return 0;
}
