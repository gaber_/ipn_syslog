#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/un.h>

#include <sys/ioctl.h>

#include <pwd.h>
#include <errno.h>
#include <signal.h>
#include <getopt.h>

#include "af_ipn.h"
#include "ipn_syslog.h"

#define DEV_PATH "/dev/log"
#define IPN_PATH IPN_SYSLOG_SOCK

static int dev_fd, ipn_fd;

static int setup_unix_fd(const char *path, int dom, int type, int proto, mode_t mode) {
	int rc, fd;
	struct sockaddr_un addr;

	memset(&addr, 0, sizeof(addr));
	addr.sun_family = dom;
	strcpy(addr.sun_path, path);

	fd = socket(dom, type, proto);

	if (fd < 0) {
		perror("socket()");
		exit(-1);
	}

	rc = bind(fd, (struct sockaddr *) &addr, sizeof(addr));

	if (rc < 0) {
		perror("bind()");
		exit(-1);
	}

	rc = chmod(path, mode);

	if (rc < 0) {
		perror("chmod()");
		exit(-1);
	}

	return fd;
}

static void close_unix_fd(const char *path, int fd) {
	int rc;

	rc = close(fd);

	if (rc < 0) {
		perror("close()");
		exit(-1);
	}

	rc = unlink(path);

	if (rc < 0) {
		perror("unlink()");
		exit(-1);
	}
}

static int syslogd_process(int fd) {
	int rc;
	ssize_t len;
	char buf[IPN_SYSLOG_MAXRCV];

	len = read(dev_fd, buf, IPN_SYSLOG_MAXRCV);

	if (len < 0) {
		perror("read()");
		exit(-1);
	}

	rc = send(ipn_fd, buf, len, 0);

	if (rc < 0) {
		perror("send()");
		exit(-1);
	}

	return len;
}

static void syslogd_loop() {
	int rc;
	fd_set rfds;
	ssize_t len;

	do {
		FD_ZERO(&rfds);

		FD_SET(dev_fd, &rfds);

		rc = select(FD_SETSIZE, &rfds, NULL, NULL, NULL);

		if (rc < 0) {
			perror("select()");
			exit(-1);
		}

		if (FD_ISSET(dev_fd, &rfds))
			len = syslogd_process(dev_fd);

	} while (len > 0);
}

static void syslogd_exit() {
	close_unix_fd(DEV_PATH, dev_fd);
	close_unix_fd(IPN_PATH, ipn_fd);
}

static void syslogd_signal(int sig) {
	exit(0);
}

static void syslogd_help() {
	#define CMD_HELP(CMDL, CMDS, MSG) printf("  %s, %s\t%s.\n", CMDS, CMDL, MSG);

	puts("Usage: ipn_syslogd [OPTIONS]\n");
	puts(" Options:");

	CMD_HELP("--user",	"-u",	"chown the IPN socket to this user");
	CMD_HELP("--help",	"-h",	"Show this help");

	puts("");
}

int main(int argc, char *argv[]) {
	int rc, opt, i;
	uid_t uid = 0, gid = 0;

	struct sigaction sa;

	/* TODO: support for UDP (RFC5426) */
	/* TODO: support for TCP (RFC3195) */
	/* TODO: support for SSL (RFC5425) using GnuTLS */

	static struct option opts[] = {
		{ "user", required_argument, 0, 'u' },
		{ "help", no_argument,       0, 'h' },
		{ 0,      0,                 0, 0   }
	};

	while ((opt = getopt_long(argc, argv, "u:h", opts, &i)) >= 0) {
		switch (opt) {
			case 'u': {
				struct passwd *pwd = getpwnam(optarg);

				if (pwd == NULL) {
					perror("getpwnam()");
					exit(-1);
				}

				uid = pwd -> pw_uid;
				gid = pwd -> pw_gid;
				break;
			}

			case 'h': {
				syslogd_help();
				exit(-1);
			}
		}
	}

	dev_fd = setup_unix_fd(DEV_PATH, AF_UNIX, SOCK_DGRAM, 0, 0666);
	ipn_fd = setup_unix_fd(IPN_PATH, AF_IPN, SOCK_RAW, IPN_SYSLOG, 0642);

	chown(IPN_PATH, uid, gid);

	atexit(syslogd_exit);

	sa.sa_flags   = 0;
	sa.sa_handler = syslogd_signal;

	rc = sigaction(SIGINT, &sa, NULL);

	if (rc < 0) {
		perror("sigaction()");
		exit(-1);
	}

	rc = shutdown(ipn_fd, SHUT_RD);

	if (rc < 0) {
		perror("shutdown()");
		exit(-1);
	}

	rc = connect(ipn_fd, NULL, 0);

	if (rc < 0) {
		perror("connect()");
		exit(-1);
	}

	syslogd_loop();

	return 0;
}
