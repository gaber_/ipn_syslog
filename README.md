Syslog switch for IPN
=====================

**ipn_syslog** is an IPN submodule which implements a policy for the syslog
(RFC5424) protocol. It allows applications to receive syslog messages matching
particular facilities (e.g. "cron", "mail", "user", ...) and severities (e.g.
"warn", "debug", "info", ...) (chosen by the applications themselves) using an
IPN network.

It is composed of three parts:

 * `ipn_syslogd` is a userspace daemon which receives syslog messages from local
   applications (called originators, i.e. those using `syslog(3)`) and forwards
   them to the IPN network.
 * `ipn_syslog.ko` is the kernel module which actually implements the syslog
   policy.
 * `ipn_syslog.so` is a library implementing the syslog API which can be used to
   access the IPN socket directly (in which case the `ipn_syslogd` would only be
   needed for creating/deleting the IPN socket).

Applications that want to receive messages from ipn_syslog need to:

 * Create a new socket using the `IPN_SYSLOG` value (defined in the
   `ipn_syslog.h` file) as protocol:

```c
     int fd = socket(AF_IPN, SOCK_RAW, IPN_SYSLOG);
```

 * Connect to the ipn_syslog unix socket (`/tmp/ipn_syslog.sock` by default).
 * Register for particular syslog facilities/severities using `ioctl(2)`:

```c
     /* register for the LOG_USER and LOG_DAEMON facilities */
     ioctl(fd, IPN_IOCTL_FACILITY, LOG_USER);
     ioctl(fd, IPN_IOCTL_FACILITY, LOG_DAEMON);

     /* register for the messages having LOG_CRITICAL severity or above */
     ioctl(fd, IPN_IOCTL_SEVERITY, LOG_CRITICAL);
```

## SECURITY

Syslog messages can contain sensitive information (e.g. related to users
authentication), it is therefore important to restrict the applications and
users that can access them.

In ipn_syslog this is accomplished by assigning the proper permission to the
IPN socket: the ipn_syslogd daemon uses the file mode `0642` when creating the
IPN socket, meaning that only applications having the same GID can read from it.

For convenience, ipn_syslogd supports the optional `--user` option, whose value
is used to chown the socket when created.

## BUILD

The following command will clone the ipn repository, build it and then will
build ipn\_syslog (requires `svn(1)`):

```bash
$ make
```

If a local build of the IPN repository is already present in another path, the
following command can be used:

```bash
$ make IPN_PATH=/path/to/ipn
```

## USAGE

* Load the kernel module (assuming the ipn module has already been loaded):

```bash
$ sudo insmod ipn_syslog.ko
```

### Relayed access

* Start the IPN syslog relay:

```bash
$ sudo ./ipn_syslogd -u daemon
```

* Optionally start the kernel log daemon (e.g. busybox'):

```bash
$ sudo klogd
```

* Start the collector(s) with group `daemon`:

```bash
$ sudo -g daemon ./collector -f user -s notice
$ sudo -g daemon ./collector -f deamon -f mail -s error
[...]
```

* Run the originator(s):

```bash
$ ./originator -f user -s crit -m "critical message"
$ ./originator -f daemon -s debug -m "debug message"
[...]
```

### Direct access

* Start the IPN syslog relay (it's only needed to manage the IPN socket):

```bash
$ sudo ./ipn_syslogd -u daemon
```

* Start the collector(s) with group `daemon`:

```bash
$ sudo -g daemon ./collector -f user -s notice
$ sudo -g daemon ./collector -f deamon -f mail -s error
[...]
```

* Run the originator(s) using the `ipn_syslog.so` library:

```bash
$ LD_PRELOAD=./ipn_syslog.so ./originator -f user -s crit -m "critical message"
$ LD_PRELOAD=./ipn_syslog.so ./originator -f daemon -s debug -m "debug message"
[...]
```
