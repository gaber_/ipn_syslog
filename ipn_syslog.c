#include <linux/module.h>
#include <linux/ctype.h>

#include "af_ipn.h"
#include "ipn_syslog.h"

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Alessandro Ghedini and Gaber Ayoubi");
MODULE_DESCRIPTION("Syslog switch for IPN");

int facilities[] = {
	1<<0,  1<<1,  1<<2,  1<<3,  1<<4,  1<<5,  1<<6,  1<<7,  1<<8,  1<<9,
	1<<10, 1<<11, 1<<12, 1<<13, 1<<14, 1<<15, 1<<16, 1<<17, 1<<18, 1<<19,
	1<<20, 1<<21, 1<<22, 1<<23
};

struct ipn_syslog_pri {
	unsigned long facilities;
	unsigned long severities;
};

static int syslog_parse_msg(const unsigned char *msg, int *fac, int *sev) {
	int i, pri = 0;

	if (msg[0] != '<')
		return -1;

	for (i = 1; msg[i] != '>'; i++) {
		if (isdigit(msg[i]))
			pri = (msg[i] - 48) + 10 * pri;
		else
			return -1;
	}

	*sev = pri % 8;
	*fac = (pri - *sev) / 8;

	return 0;
}

static int syslog_newport(struct ipn_node *node) {
	struct ipn_syslog_pri *entry =
		kmalloc(sizeof(struct ipn_syslog_pri), GFP_KERNEL);

	entry -> facilities = 0;
	entry -> severities = 5; /* LOG_NOTICE */

	node -> proto_private = entry;

	printk("IPN_SYSLOG: new node connected\n");

	return 0;
}

static void syslog_delport(struct ipn_node *node) {
	kfree(node -> proto_private);
}

static int syslog_handlemsg(struct ipn_node *node, struct msgpool_item *msg) {
	struct ipn_node *curr;
	int fac = 0, sev = 0, rc;

	rc = syslog_parse_msg(msg -> data, &fac, &sev);

	if (rc == -1) {
		printk("IPN_SYSLOG: invalid log\n");
		return -EINVAL;
	}

	/* TODO: use table to improve performance */
	list_for_each_entry(curr, &node -> ipn -> connectqueue, nodelist) {
		struct ipn_syslog_pri *entry = curr -> proto_private;

		if ((entry -> facilities & facilities[fac]) &&
		    (entry -> severities >= sev)) {
			ipn_proto_sendmsg(curr, msg);
		}
	}

	return 0;
}

static int syslog_newnet(struct ipn_network *net) {
	printk("IPN_SYSLOG: created network\n");
	return 0;
}

static int syslog_ioctl(struct ipn_node *node, unsigned req, unsigned long arg) {
	struct ipn_syslog_pri *entry = node -> proto_private;
	unsigned long fac = arg>>3;

	switch (req) {
		case IPN_IOCTL_FACILITY:
			entry -> facilities |= facilities[fac];
			printk("IPN_SYSLOG: registered facility %ld\n", fac);
			break;

		case IPN_IOCTL_SEVERITY:
			printk("IPN_SYSLOG: registered severity %ld\n", arg);
			entry -> severities = arg;
			break;

		default:
			return -EINVAL;
	}

	return 0;
}

static struct ipn_protocol syslog_proto = {
	.ipn_p_newport   = syslog_newport,
	.ipn_p_delport   = syslog_delport,
	.ipn_p_handlemsg = syslog_handlemsg,
	.ipn_p_newnet    = syslog_newnet,
	.ipn_p_ioctl     = syslog_ioctl
};

static int __init init_ipn_syslog(void) {
	return ipn_proto_register(IPN_SYSLOG, &syslog_proto);
}

static void __exit exit_ipn_syslog(void) {
	ipn_proto_deregister(IPN_SYSLOG);
}

module_init(init_ipn_syslog);
module_exit(exit_ipn_syslog);
